# Data processing Problem 2  #
- Generate a csv file containing first_name, last_name, address, date_of_birth
- Process the csv file to anonymise the data
- Columns to anonymise are first_name, last_name and address
- You might be thinking  that is silly
- Now make this work on 2GB csv file (should be doable on a laptop)
- Demonstrate that the same can work on bigger dataset
- Hint - You would need some distributed computing platform

## Technologies ##

- Spring boot 2.3.4, running platform 
- Apache Camel 3.5, integration platform
- Kafka 2.2.0, message queue
- Gradle 6.5, build tool
- Language, Groovy 2.5.13
- Encryption, java security crypto 

## Data processing Detailed Design ##

1. Once receive JSON message from the topic create.user.profile, Transform json to POJO
    1. Input: UserProfile JSON
    2. Output: UserProfile POJO
2. Enrichment of the message
    1. Columns to anonymise are first_name, last_name and address
    2. Apply AES encryption algorithm
3. Transform UserProfile POJO to CSV content
    1. Input: UserProfile POJO
    2. Output: UserProfile CSV
4. Write/Append csv content to the file
    1. Filename format: yyyyMMddHH.csv
    2. One file per hour
      

## Code Explanation ###
1. Service to process csv data: com.data.processing.v1.route.CreateUserProfileRoute
2. Encryption Util: com.data.processing.v1.util.CipherData


## Testing ##
1. Publish JSON UserProfile message to the topic
    1. Request: {"firstName":"jack1","lastName":"Smith", "address":"12, Queens St, Melbourne, VIC, 3000", "dateOfBirth":"15/09/1987"}
    2. To the file: "0eSYUFJr889x2eOgJvFR2g==","zFFzZu0v111YdfqeSdwmgA==","lsDDQZJb1mNFrI0b4Bu9e/enH+/ZxFrzYrrx7ypnSMYXb0cQw6oFtZ0/kG3CYqoK","15/09/1987"