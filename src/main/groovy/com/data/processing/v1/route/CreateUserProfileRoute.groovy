package com.data.processing.v1.route

import static com.data.processing.v1.util.CipherData.instance

import com.data.processing.v1.dataformat.UserProfile
import com.data.processing.v1.util.CipherData
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.camel.LoggingLevel
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.jackson.JacksonDataFormat
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat
import org.apache.camel.spi.DataFormat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import java.text.MessageFormat

/**
 *
 * Generate a user profile CSV file per hour
 *
 * @author Neo Xu - 2020-09-08
 */
@Component
class CreateUserProfileRoute extends RouteBuilder {
	
	private static final String LOG_PREFIX = 'data-processing-v1.create-user-profile'

	private DataFormat userDataFormat = new BindyCsvDataFormat(UserProfile)
	private DataFormat userJson = new JacksonDataFormat(new ObjectMapper(), UserProfile)
	

	@Value('${create-user-profile.topic}')
	private String topic

	@Value('${create-user-profile.directory}')
	private String directory

	@Value('${spring.kafka.consumer.group-id}')
	private String kafkaGroupId

	@Value('${spring.kafka.bootstrap-servers}')
	private String kafkaBroker

	@Autowired
	private CipherData cipherData

	private String consumerEndpoint

	@Override
	void configure() throws Exception {
		
		from(consumerEndpoint?:MessageFormat.format('kafka:{0}?brokers={1}&groupId={2}', topic, kafkaBroker, kafkaGroupId))
			.routeId('createUserProfileRoute')
			.log(LoggingLevel.INFO, LOG_PREFIX + '.request', '${body}')

//		    parse json
			.unmarshal(userJson)
		
//			encrypt first
			.process {
				def user = it.in.body as UserProfile
				if (user) {
					user.setFirstName(getInstance().encrypt(user.getFirstName()))
					user.setLastName(getInstance().encrypt(user.getLastName()))
					user.setAddress(getInstance().encrypt(user.getAddress()))
				}
			}
		
//			generate csv and append to the file
			.marshal(userDataFormat)
			.convertBodyTo(String)
			.log(LoggingLevel.INFO, LOG_PREFIX + '.append.file', '${body}')
			.to(MessageFormat.format('file:{0}?fileName={1}&fileExist={2}', directory, '${date:now:yyyyMMddHH}.csv', 'Append'))
	}
}
