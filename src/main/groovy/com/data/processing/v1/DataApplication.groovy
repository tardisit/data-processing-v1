package com.data.processing.v1

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class DataApplication {

	static void main(String[] args) {
		SpringApplication.run(DataApplication, args)
	}

}
