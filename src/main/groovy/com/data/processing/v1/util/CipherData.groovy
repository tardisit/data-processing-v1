package com.data.processing.v1.util

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
import javax.xml.bind.DatatypeConverter
import java.security.Key

/**
 * 
 * Encrypt and Decrypt given value
 *
 * @author Neo Xu - 2020-09-09
 */
@Component
class CipherData {
	private static final String ALGORITHM = 'AES'
	private static CipherData instance = null

	@Value('${cipher-data.key}')
	String keyValue

	private Key key

	// private constructor restricted to this class itself 
	private CipherData() {
		if (!keyValue) {
			keyValue = 'XDAF@#FDFASf1d2d'
		}
		key = new SecretKeySpec(keyValue.getBytes('UTF-8'), ALGORITHM)
	}

	// static method to create instance of Singleton class 
	static CipherData getInstance() {
		if (instance == null) {
			instance = new CipherData()
		}
		return instance
	}

	String encrypt(String valueToEnc) throws Exception {
		Cipher cipher = Cipher.getInstance(ALGORITHM)
		cipher.init(Cipher.ENCRYPT_MODE, key)
		byte[] encodedValue = cipher.doFinal(valueToEnc.getBytes())
		String encryptedValue = DatatypeConverter.printBase64Binary(encodedValue)
		return encryptedValue
	}

	/**
	 * @param encryptedValue
	 * @return
	 * @throws Exception
	 */
	String decrypt(String encryptedValue) throws Exception {
		Cipher cipher = Cipher.getInstance(ALGORITHM)
		cipher.init(Cipher.DECRYPT_MODE, key)
		byte[] decodedValue = DatatypeConverter.parseBase64Binary(encryptedValue)
		byte[] decodeValue = cipher.doFinal(decodedValue)
		String decryptedValue = new String(decodeValue)
		return decryptedValue
	}


}