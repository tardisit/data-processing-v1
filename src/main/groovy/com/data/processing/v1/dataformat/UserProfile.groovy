package com.data.processing.v1.dataformat

import org.apache.camel.dataformat.bindy.annotation.CsvRecord
import org.apache.camel.dataformat.bindy.annotation.DataField


/**
 * UserProfile Common Model to store input and output value
 *
 * @author Neo Xu - 2020-09-08
 */
@CsvRecord(separator = ',', quoting = true, generateHeaderColumns = false)
class UserProfile {

	@DataField(pos = 1, required = true, name = 'first_name', trim = true)
	String firstName

	@DataField(pos = 2, required = true, name = 'last_name', trim = true)
	String lastName

	@DataField(pos = 3, required = true, name = 'address', trim = true)
	String address

	@DataField(pos = 4, required = true, name = 'date_of_birth', trim = true)
	String dateOfBirth
}
