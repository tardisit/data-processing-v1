package com.data.processing.v1.route

import static java.time.format.DateTimeFormatter.ofPattern
import static java.util.concurrent.TimeUnit.SECONDS

import org.apache.camel.Endpoint
import org.apache.camel.EndpointInject
import org.apache.camel.Exchange
import org.apache.camel.RoutesBuilder
import org.apache.camel.builder.NotifyBuilder
import org.apache.camel.support.DefaultExchange
import org.apache.camel.test.junit4.CamelTestSupport
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

/**
 * 
 *
 * @author Neo Xu - 2020-09-09
 */
class CreateUserProfileRouteTest extends CamelTestSupport {

	private static final DateTimeFormatter FILE_DATE_TIME_FORMAT = ofPattern('yyyyMMddHH')
	
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder()

	@EndpointInject('direct:kafkaConsumer')
	Endpoint createUserProfileEndpoint
	
	Exchange exchange
	String outputDirectory
	String filename

	@Before
	void doBefore() {
		exchange = new DefaultExchange(context)
		filename = FILE_DATE_TIME_FORMAT.format(ZonedDateTime.now()) + '.csv'
	}

	@Override
	protected void doPreSetup() throws Exception {
		outputDirectory = temporaryFolder.newFolder('data')
	}
	
	@Override
	protected RoutesBuilder createRouteBuilder() throws Exception {
		return new CreateUserProfileRoute(
			topic: 'data',
			kafkaGroupId: 'id',
			kafkaBroker: 'localhost:9092',
			directory: outputDirectory,
			consumerEndpoint: 'direct:kafkaConsumer'
		)
	}
	
	@Test
	void testInvalidData() {
		exchange.in.body = 'jack1'

		def notify = new NotifyBuilder(context).whenCompleted(1).create()
		template.send(createUserProfileEndpoint, exchange)

		notify.matches(6, SECONDS)
		assertMockEndpointsSatisfied()

		assert !new File(outputDirectory, filename).exists()
	}

	@Test
	void testSuccessData() {
		exchange.in.body = '{"firstName":"jack1","lastName":"Smith", "address":"12, Queens St, Melbourne, VIC, 3000", "dateOfBirth":"15/09/1987"}'

		def notify = new NotifyBuilder(context).whenCompleted(1).create()
		template.send(createUserProfileEndpoint, exchange)

		notify.matches(6, SECONDS)
		assertMockEndpointsSatisfied()

		assert new File(outputDirectory, filename).exists()
		assertEquals('"0eSYUFJr889x2eOgJvFR2g==","zFFzZu0v111YdfqeSdwmgA==","lsDDQZJb1mNFrI0b4Bu9e/enH+/ZxFrzYrrx7ypnSMYXb0cQw6oFtZ0/kG3CYqoK","15/09/1987"',
			new File(outputDirectory, filename).text.replaceAll('\\r|\\n', ''))
			
	}
}