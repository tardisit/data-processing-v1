package com.data.processing.v1.util

import org.junit.Assert
import org.junit.Test

import javax.crypto.IllegalBlockSizeException

/**
 *
 *
 *
 * @author Neo Xu - 2020-09-09
 */
class CipherDataTest {

	@Test(expected = NullPointerException)
	void testEncryptException() {
		CipherData.getInstance().encrypt(null)
	}
	
	@Test
	void testEncrypt() {
		Assert.assertNotEquals('John', CipherData.getInstance().encrypt('John'))
		Assert.assertEquals('B8lL28kT+tGDnZag0TytrA==', CipherData.getInstance().encrypt('John'))
	}

	@Test(expected = IllegalBlockSizeException)
	void testDecryptException() {
		CipherData.getInstance().decrypt('queens street')
	}

	@Test
	void testDecryptSuccess() {
		Assert.assertEquals('queens street', CipherData.getInstance().decrypt('Ts6kgOUHaqPeWSsLOY1+Fw==')) 
	}


}